# Ansible Role: Opengear Console Server

A quick and dirty ansible role to configure an [Opengear IM7200 console server](https://opengear.com/products/im7200-infrastructure-manager/). Note this is just a working example, it will take take customisation for it to work properly with you ansible environment.

Two notes:

- The users are authenticating via ssh keys only. No WebUI access is allowed in this example setup.
- Idempotency is a bit of a challenge. For example, to allow the creation of additional users over time, this role efectively overwrites all users except the `root` user. So adapt this to your level of comfort!

## Requirements

- Tested on Ubuntu 20.04

## Role Variables

Take a look at the `group_vars` and `host_vars` directories for example variables.

```yaml
opengear_wan_ipv4:
    address: 10.0.0.251
    netmask: 255.255.255.0
    gateway: 10.0.0.1
    alias: "OOB"

ports:
    port1:
        label: DEVICE-NAME
        loglevel: 2
        speed: 9600
        ssh: !!str on
        webshell: !!str off

groups:
    network-admin:
        users:
            - ipsum
            - lorem

syslog:
    servers:
        - 10.0.0.20

dns:
    servers:
        ipv4:
            - 1.1.1.1
```

## Usage

```yaml
ansible-playbook deploy_opengear.yml
```

## License

[Unlicense](https://unlicense.org)

## Author Information

This role was created in 2021 by [Pete Crocker](http://petecrocker.com/).
